###################
# DEPLOYMENTS     #
###################
  - name: Create drupal builder deployment config
    k8s:
      definition:
        kind: DeploymentConfig
        apiVersion: apps.openshift.io/v1
        metadata:
          labels:
            app: d8-infra
          name: drupal-builder
          namespace: '{{ namespace }}'
        spec:
          replicas: 1
          selector:
            app: d8-infra
            deploymentconfig: drupal-builder
          strategy:
            #https://docs.openshift.com/container-platform/3.9/dev_guide/deployments/deployment_strategies.html#when-to-use-a-recreate-deployment
            type: Rolling
            rollingParams:        
              timeoutSeconds: 1200
              post:
                failurePolicy: Retry
                execNewPod:
                  containerName: php-fpm 
                  command: ["/bin/sh","-c","drush status && /usr/libexec/s2i/post.sh"]
          template:
            metadata:
              labels:
                app: d8-infra
                deploymentconfig: drupal-builder
            spec:
              containers:
              - name: php-fpm
                image: 'drupal-builder:latest'
                imagePullPolicy: Always
                ports:
                - containerPort: 9000
                  protocol: TCP
                env:
                  - name: NAMESPACE
                    valueFrom:
                      fieldRef:
                        apiVersion: v1
                        fieldPath: metadata.namespace

                  - name: HOSTNAME
                    value: $(NAMESPACE).web.cern.ch

                  - name: PROFILE
                    value: "{{ apb_profile_value | default('cern',true) }}"

                  - name: DB_HOST
                    valueFrom:
                      configMapKeyRef:
                        key: DB_HOST_KEY
                        name: global-env-configmap

                  - name: DB_PORT
                    valueFrom:
                      configMapKeyRef:
                        key: DB_PORT_KEY
                        name: global-env-configmap

                  - name: DB_NAME
                    valueFrom:
                      configMapKeyRef:
                        key: DB_NAME_KEY
                        name: global-env-configmap

                  - name: DB_USER
                    valueFrom:
                      secretKeyRef:
                        name: global-env-secrets
                        key: DB_USER_KEY

                  - name: DB_PASSWORD
                    valueFrom:
                      secretKeyRef:
                        name: global-env-secrets
                        key: DB_PASSWORD_KEY

                  - name: AUTH_ADMINPASSWORD
                    valueFrom:
                      secretKeyRef:
                        name: global-env-secrets
                        key: SIMPLESAMLPHP-AUTH-ADMINPASSWORD_KEY

                  - name: SECRETSALT
                    valueFrom:
                      secretKeyRef:
                        name: global-env-secrets
                        key: SIMPLESAMLPHP-SECRETSALT_KEY

                resources:
                  limits:
                    memory: 1Gi
                    cpu: 500m
                  requests:
                    memory: 64Mi
                    cpu: 20m
                volumeMounts:
                  - mountPath: /private
                    name: private-drupal-files
                  - mountPath: /var/www/html/web/sites/default/files
                    name: public-drupal-files
                  - mountPath: /tmp/phpfpm-configmap
                    name: phpfpm-configmap
                    readOnly: true
                  - mountPath: /tmp/opcache-configmap
                    name: opcache-configmap
                    readOnly: true
                  - mountPath: /tmp/simplesamlphp-configmap
                    name: simplesamlphp-configmap
                    readOnly: true
                  - mountPath: /tmp/simplesamlcert
                    name: certs

              - name: nginx
                image: 'drupal-builder:latest'
                imagePullPolicy: Always
                command:
                  - sh
                  - '-c'
                  - /usr/libexec/s2i/run-nginx.sh
                ports:
                - containerPort: 8080
                  protocol: TCP
                resources:
                  limits:
                    memory: 1Gi
                    cpu: 500m
                  requests:
                    memory: 8Mi
                    cpu: 1m
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
                volumeMounts:
                  - mountPath: /private
                    name: private-drupal-files
                  - mountPath: /var/www/html/web/sites/default/files
                    name: public-drupal-files
                  - mountPath: /tmp/nginx-configmap
                    name: nginx-configmap              
                    readOnly: true
              dnsPolicy: ClusterFirst
              restartPolicy: Always
              securityContext: {}
              volumes:
                - name: private-drupal-files
                  persistentVolumeClaim:
                    claimName: private-drupal-files
                - name: public-drupal-files
                  persistentVolumeClaim:
                    claimName: public-drupal-files
                - configMap:
                    defaultMode: 420
                    name: phpfpm-configmap
                  name: phpfpm-configmap
                - configMap:
                    defaultMode: 420
                    name: opcache-configmap
                  name: opcache-configmap
                - configMap:
                    defaultMode: 420
                    name: simplesamlphp-configmap
                  name: simplesamlphp-configmap
                - configMap:
                    defaultMode: 420
                    name: nginx-configmap
                  name: nginx-configmap
                - name: certs
                  secret:
                    defaultMode: 420
                    secretName: certs
          triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
              - php-fpm
              - nginx
              from:
                kind: ImageStreamTag
                name: drupal-builder:latest